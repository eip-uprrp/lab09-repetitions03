#Lab. 9: Estructuras de repetición y arreglos

Una de las ventajas de utilizar programas de computadoras es que podemos realizar tareas repetitivas fácilmente. Los ciclos  `for`, `while`, y `do-while` son un tipo de estructura de control que nos permite repetir un conjunto de instrucciones. A estas estructuras también les llamamos *estructuras de repetición*. 

Los arreglos nos permiten organizar y acceder datos de manera sistemática utilizando ciclos.


##Objetivos:

En esta experiencia de laboratorio los estudiantes se expondrán a algoritmos de procesamiento de sonido, simples pero ingeniosos, para practicar el uso de ciclos en la manipulación de arreglos. También practicarán el uso de arreglos y programación modular.

Esta experiencia de laboratorio es una adaptación de un "nifty assigment" presentado por Daniel Zingaro en http://nifty.stanford.edu/2012/zingaro-stereo-sound-processing/.

##Pre-Lab:

Antes de llegar al laboratorio cada estudiante debe:

1. haber repasado los conceptos relacionados a estructuras de repetición y arreglos.

2. haber estudiado los miembros `left` y `right` de la clase `QAudioBuffer::S16S` de  la librería multimedios de `Qt`.

3. haber estudiado los conceptos e instrucciones para la sesión de laboratorio.

4. haber tomado el [quiz Pre-Lab 9](http://moodle.ccom.uprrp.edu/mod/quiz/view.php?id=6828) (recuerda que el quiz Pre-Lab puede tener conceptos explicados en las instrucciones del laboratorio).


##Procesamiento de sonido digital

El sonido es una vibración que se propaga en medios elásticos tales como el aire, el agua y los sólidos. Las ondas sonoras son generadas por una fuente de sonido como por ejemplo la vibración del diafragma de una bocina de sonido [2]. Las ondas de sonido consisten de segmentos de alta y baja presión llamados respectivamente compresiones y rarefacciones.

Los micrófonos convierten las ondas de sonido a señales eléctricas. Estas señales eléctricas pueden digitalizarse, o sea, pueden ser convertidas a sucesiones de números, en donde cada número representa la intensidad de la señal eléctrica en un momento en el tiempo. La *razón de la muestra* es el número de muestras de la señal de sonido tomadas en un segundo. Por ejemplo, para obtener la calidad de sonido de una grabación estéreo de CD se usa una razón de muestra de 44,100 muestras por segundo. Esto significa que en cada segundo, para cada uno de los canales (izquierdo y derecho), se toman 44,100 muestras de sonido y se convierten a números.

 <div align='center'><img src="http://homepages.udayton.edu/~hardierc/ece203/sound_files/image001.jpg"></div>


**Figura 1.** Ilustración de los pasos para realizar una digitalización de sonido. El micrófono convierte la onda de presión en una señal de voltaje. El convertidor de señal análoga a señal digital toma muestras de la señal de voltaje y convierte cada muestra en un valor numérico. La sucesión de números forma el sonido *digitalizado*. Tomado de http://homepages.udayton.edu/~hardierc/ece203/sound_files/image001.jpg.

----

**Pregunta...**

Suponga que cada muestra de sonido se convierte a un número que ocupa 2 bytes. ?Cuántos bytes se necesitan para guardar una canción que dure exactamente 180 segundos y que fue grabada en calidad de CD estéreo? 

**Respuesta:**

180 segundos * 44,100 muestras/segundo * 2 bytes/muestra * 2 canales = 31,752,000 bytes = 31.75 MBytes.

Afortunadamente existen técnicas de compresión de datos como *MP3* y *ogg* que reducen la cantidad de memoria necesaria para guardar música con calidad de CD.

----

Las **técnicas de procesamiento digital** se pueden utilizar para mejorar la calidad del sonido removiendo ruido y eco,  para comprimir los datos y para mejorar la transmisión. El procesamiento de sonido digital también juega un papel importante en las aplicaciones de reconocimiento de voz y en investigaciones científicas de detección de biodiversidad utilizando sensores de sonido [3]. El sonido digital también se puede manipular fácilmente para lograr efectos especiales.

Como las grabaciones de sonido digital son escencialmente una colección de valores numéricos que representan una onda de sonido, el procesamiento de sonido digital puede ser tan simple como el aplicar operaciones aritméticas a esos valores. Por ejemplo, digamos que tienes una grabación de sonido digital. Mientras más alto el volúmen de la grabación, más altos los valores absolutos de los números que contiene. Para reducir el volúmen de toda la grabación solo tendríamos que multiplicar cada valor en la grabación por un número positivo menor que 1.



 <div align='center'><img src="http://i.imgur.com/xE4kcFG.png"></div>

**Figura 2.** Una de las tareas más simples en el  procesamiento de sonido digital: cambiar el volúmen de una onda de sonido multiplicando cada punto por un valor positivo menor que 1 (en este caso 0.5).


##Bibliotecas

Para esta experiencia de laboratorio usaremos librerías multimedios de `Qt`. Para poder trabajar los ejercicios necesitarás conocer los miembros `left` y `right` de la clase `QAudioBuffer::S16S`. Para propósito de este laboratorio utilizamos el nombre `AudioBuffer` al referirnos a `QAudioBuffer::S16S`.

Cada objeto de la clase `AudioBuffer` tendrá variables miembro `left` y `right` que contienen el valor izquierdo y derecho de la muestra de sonido estéreo. Estas variables son públicas y podrás acceder su contenido escribiendo el nombre del objeto, seguido de un punto y luego el nombre de la variable. Para representar una señal de sonido, usamos un arreglo de objetos `AudioBuffer`. Cada objeto contiene los valores izquierdo y derecho  de la señal en un instante en el tiempo. Por ejemplo, si tenemos un arreglo de objetos `AudioBuffer`, llamado `frames`, entonces  `frames[i].left`  se refiere al valor del canal izquierdo del sonido en la muestra `i`.

 <div align='center'><img src="http://i.imgur.com/qTwsobr.png"></div>


**Figura 3.** En la figura, `frame` es un arreglo de objetos `AudioBuffer`. En este ejercicio de laboratorio, las señales de sonido estarán representadas por un arreglo de objetos `AudioBuffer`. Un objeto con índice `i` guarda el valor de los canales izquierdo y derecho de la muestra `i`. 

La función en el siguiente ejemplo ilustra cómo leer y modificar un arreglo de objetos `AudioBuffer`:


```cpp
void HalfVolume(AudioBuffer frames[], int N){

    // para cada muestra en la senal, reduce su valor a la mitad

    for (int i=0; i < N; i++) {
        frames[i].left  = frames[i].left / 2;
        frames[i].right = frames[i].right / 2; 
    }
}

```


##Sesión de laboratorio

1.  Ve a la pantalla de terminal y escribe el comando `git clone https://bitbucket.org/eip-uprrp/lab09-repetitions03.git` para descargar la carpeta `Lab09-Repetitions03` a tu computadora. La carpeta `SoundProcessing` contiene el esqueleto de una aplicación para hacer procesamiento de sonido estéreo. La aplicación permite al usuario aplicar cuatro algoritmos diferentes para procesamiento de sonidos. La carpeta contiene una sub-carpeta llamada `WaveSamples` con archivos de onda para que pruebes los algoritmos.

2.  Marca doble "click" en el archivo `Sounds.pro` para cargar este proyecto a Qt.


### **Ejercicio 1: Remover las voces**

Una forma barata (pero muchas veces inefectiva) de remover las voces de una grabación es tomando ventaja del hecho de que las voces usualmente se graban en ambos canales, izquierdo y derecho, mientras que el resto de los instrumentos quizás no. Si este fuera el caso, podemos remover las voces de una grabación restando el canal izquierdo y derecho.

Corre el proyecto de `Qt`. Carga cualquiera de los archivos de onda  `love.wav`, `cartoon.wav`, o `grace.wav` marcando el botón de búsqueda (`Search`) en el lado derecho de la etiqueta `Audio In`, y reprodúcela marcando el botón `Play Audio In`.

Tu tarea en este ejercicio será completar la función `RemoveVocals` que se encuentra en el archivo  `audiomanip.h` para que remueva las voces de una grabación. La función recibe un arreglo de objetos de la clase `AudioBuffer` y el tamaño del arreglo. 

**Algoritmo:**

Para cada muestra en el arreglo, computa la diferencia la muestra del canal izquierdo menos el derecho, divídelo por 2 y usa este valor como el nuevo valor para la muestra correspondiente en el canal izquierdo y derecho.

Marca el botón `Play Audio Out` en la aplicación  para reproducir el sonido del archivo de salida.

**Entrega**

Copia la función `RemoveVocals` en la sección correspondiente de la página de [Entregas del Lab 9](http://moodle.ccom.uprrp.edu/mod/quiz/view.php?id=6829). Recuerda comentar adecuadamente la función y usar buenas prácticas de indentación y selección de nombres para las variables.


### **Ejercicio 2: Intensificar**

Un efecto de sonido común es la intensificación gradual del volúmen de una grabación. Esto se consigue aumentando constantemente el valor de muestras consecutivas en el arreglo de muestras de sonido.

Para el ejercicio 2, carga y reproduce cualquiera de los archivos de onda `rain.wav`, o `water.wav` como hiciste en el Ejercicio 1. 

Tu tarea en este ejercicio es completar la función `AudioFadeIn` que se encuentra en el archivo `audiomanip.h` de modo que se intensifique gradualmente el volúmen de una grabación hasta cierto punto. La función recibe un arreglo de objetos de la clase  `AudioBuffer`, el tamaño del arreglo, y un largo de duración para el aumento en intensidad (fade_length) que será aplicado a `AudioBuffer`. Por ejemplo, si `fade_length` es `88200`, el aumento en intensidad no debe afectar ninguna muestra en posición mayor o igual a `88200`.

Reproduce la grabación en los siguientes enlaces. 

* [rain-fi.wav](http://moodle.ccom.uprrp.edu/pluginfile.php/53037/mod_resource/content/2/rain.out.wav)
* [water-fi.wav](http://moodle.ccom.uprrp.edu/pluginfile.php/53040/mod_resource/content/2/water.out.wav) 
  
Las grabaciones fueron creadas utilizando el filtro de intensidad con `fade_length 88200`. Debes escuchar como el sonido del agua y la lluvia se intensifican linealmente durante los primeros dos segundos y luego se quedan en el mismo volúmen. Nota que, como estamos usando sonidos grabados a `44100` muestras por segundo, `88200` corresponde a dos segundos de grabación.

**Algoritmo:**

Para aplicar el aumento de intensidad a un sonido, multiplicamos cada muestra sucesiva por números entre `0` y `1` que van en aumento constante. Si la muestra se multiplica por `0` se silencia, si se multiplica por `1` se queda igual; si se multiplica por un valor entre `0` y `1` el volúmen se escala por ese factor. Ambos canales deben ser multiplicados por el mismo factor.

Por ejemplo, si `fade_length` es `4`, aplicaremos el filtro a las primeras 4 muestras:

 
| Número de muestra | Multiplica por factor |
|---|---|
| 0 | 0 |
| 1 | 0.25 |
| 2 | 0.5 |
| 3 | 0.75 |
| >= 4 | 1 (No modifica la muestra) |

**Entrega**

Copia la función `AudioFadeIn` en la sección correspondiente de la página de [Entregas del Lab 9](http://moodle.ccom.uprrp.edu/mod/quiz/view.php?id=6829). Recuerda comentar adecuadamente la función y usar buenas prácticas de indentación y selección de nombres para las variables.

### **Ejercicio 3: Desvanecer**

Otro efecto de sonido común es la disminución gradual del volúmen de una grabación. Esto se consigue disminuyendo constantemente el valor de muestras consecutivas en el arreglo de muestras de sonido.

Para el ejercicio 3, carga y reproduce cualquiera de los archivos de onda `rain.wav`, o `water.wav` como hiciste en los ejercicios anteriores. 

Tu tarea en este ejercicio es completar la función `AudioFadeOut` que se encuentra en el archivo `audiomanip.h` para que desvanezca el volúmen a partir de una muestra de la una grabación hasta el final. La función recibe un arreglo de objetos de la clase `AudioBuffer`, el tamaño del arreglo, y un largo de duración del desvanecimiento que será aplicado a `AudioBuffer`. Por ejemplo, si  `fade_length` es `88200`, el desvanecimiento no debe afectar ninguna muestra en  posiciones menores o iguales a `88200`. 

Reproduce la grabación en los siguientes enlaces. 

* [rain-fo.wav](http://moodle.ccom.uprrp.edu/pluginfile.php/53045/mod_resource/content/1/rain.out.wav)
* [water-fo.wav](http://moodle.ccom.uprrp.edu/pluginfile.php/53044/mod_resource/content/1/water.out.wav) 
  
Las grabaciones fueron creadas utilizando el filtro de desvanecer con `fade_length 88200`. Debes escuchar el sonido del agua y la lluvia en volúmen constante y luego, en los últimos dos segundos, el volúmen disminuye linealmente hasta desaparecer.

**Algoritmo:**

Los factores para desvanecer son los mismos que para intensificar pero se aplican en el orden opuesto. Por ejemplo, si `fade_length` fuera `4`, las muestras de los canales en la posición cuatro antes de la última se multiplican por `0.75`, las muestras de los canales en la posición tres antes de la última se multiplican por `0.5`, las muestras de los canales en la penúltima posición se multiplican por `0.25`, y las muestras en los canales en la última posición se multiplican por `0.0`. 

**Entrega**

Copia la función `AudioFadeOut` en la sección correspondiente de la página de [Entregas del Lab 9](http://moodle.ccom.uprrp.edu/mod/quiz/view.php?id=6829). Recuerda comentar adecuadamente la función y usar buenas prácticas de indentación y selección de nombres para las variables.

### **Ejercicio 4: Recorrido de izquierda a derecha**

El efecto de sonido que queremos lograr en este ejercicio es comenzar a escuchar un sonido por el canal izquierdo, que vaya desvaneciéndose en ese canal, vaya intensificándose en el canal derecho y termine completamente en el canal derecho.

Para el ejercicio 4, carga y reproduce el archivo  `airplane.wav` como hiciste en los ejercicios anteriores. 

Tu tarea en este ejercicio es completar la función `LeftToRight` que se encuentra en el archivo `audiomanip.h` para que haga que el sonido vaya "moviéndose" del canal izquierdo al canal derecho. La función recibe un arreglo de objetos de la clase `AudioBuffer`, el tamaño del arreglo, y un largo de recorrido (pan_length) que será aplicado a `AudioBuffer`. Por ejemplo, si  `pan_length` es `88200`, el recorrido no debe afectar ninguna muestra en  posiciones mayores o iguales a `88200`. 

Reproduce la grabación en el enlace [airplane-pan.wav](http://moodle.ccom.uprrp.edu/pluginfile.php/53046/mod_resource/content/1/airplane.out.wav). Debes poder oir cómo el sonido del avión se escucha primero completamente a la izquierda y luego se mueve lentamente hacia la derecha, terminando completamente a la derecha en la última muestra.

**Algoritmo:**

Para crear el efecto de que el sonido se mueve de izquerda a derecha se necesita un desvanecimiento en el canal de la izquierda y una intensificación en el canal de la derecha. Por ejemplo, si el `pan_lenght` es 4, el filtro será aplicado a las primeras 4 muestras:


 
| Número de muestra | Factor a multiplicar por canal izquierdo |Factor a multiplicar por canal derecho |
|---|---|---|
| 0 | 0.75 | 0 |
| 1 | 0.5 | 0.25 |
| 2 | 0.25 | 0.5 |
| 3 | 0 | 0.75 |
| >= 4 | (No modificar la muestra) | (No modificar la muestra) | 

**Entrega**

Copia la función `LeftToRight` en la sección correspondiente de la página de [Entregas del Lab 9](http://moodle.ccom.uprrp.edu/mod/quiz/view.php?id=6829). Recuerda comentar adecuadamente la función y usar buenas prácticas de indentación y selección de nombres para las variables.



## Referencias
[1] Daniel Zingaro, http://nifty.stanford.edu/2012/zingaro-stereo-sound-processing/

[2] http://en.wikipedia.org/wiki/Sound

[3] Arbimon, A web based network for storing, sharing, and analyzing acoustic information. http://arbimon.com/
