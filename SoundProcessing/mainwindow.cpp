#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "audiomanip.h"

MainWindow::MainWindow(QWidget *parent):
    QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

//================================================================================

void MainWindow::NoVocals(QAudioBuffer::S16S *frames)
{
    RemoveVocals(frames, ws.frameCount()) ;
}

void MainWindow::FadeIn(QAudioBuffer::S16S *frames, int fade_length)
{

    AudioFadeIn(frames, ws.frameCount(), fade_length);

}

void MainWindow::FadeOut(QAudioBuffer::S16S *frames, int fade_length)
{
    AudioFadeOut(frames, ws.frameCount(), fade_length);
}

void MainWindow::Left2Right(QAudioBuffer::S16S *frames, int pan_length)
{
    LeftToRight(frames, ws.frameCount(), pan_length);
}

//================================================================================

void MainWindow::on_actionExit_triggered()
{
  ws.~WaveSound() ;
  QApplication::quit();
}

//about
void MainWindow::on_actionAbout_triggered()
{
    //display a message box
    QMessageBox::about(this, tr("About Wave Editor"),
    tr("Wave Editor was created with Qt Creator 2.7.0 (Qt 5.01 framework)"
       "\n\nCreated by Jonathan Velez Alvarez"
       "\n\nThe program is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE WARRANTY "
       "OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE."));
}

void MainWindow::on_Filter_Box_currentIndexChanged(int index)
{
    ui->Fade_Pan_Lenght->setEnabled(bool(index));
    ui->label_3->setEnabled(bool(index));
}




//Set Input Audio File With Action
void MainWindow::on_Action_Input_Audio_File_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"), "", tr("Files (*.wav)"));
    Frames = ws.getAudioSamples(fileName);

    AudioInFile = AudioOutFile = fileName;
    AudioOutFile.truncate(AudioInFile.size()-3);
    AudioOutFile = AudioOutFile+"out.wav";

    ui->Audio_In->addItem(AudioInFile);
    ui->Audio_In->setCurrentText(AudioInFile);
    ui->Audio_Out->addItem(AudioOutFile);
    ui->Audio_Out->setCurrentText(AudioOutFile);

    ui->Play_Audio_In->setEnabled(true);
    ui->Stop_Audio_In->setEnabled(true);

    ui->Play_Audio_Out->setEnabled(false);
    ui->Stop_Audio_Out->setEnabled(false);

    ui->run_filter->setEnabled(true);
}

//Set Input Audio File With Search Button
void MainWindow::on_Search_Audio_In_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"), "", tr("Files (*.wav)"));
    Frames = ws.getAudioSamples(fileName);

    AudioInFile = AudioOutFile = fileName;
    AudioOutFile.truncate(AudioInFile.size()-3);
    AudioOutFile = AudioOutFile+"out.wav";

    ui->Audio_In->addItem(AudioInFile);
    ui->Audio_In->setCurrentText(AudioInFile);
    ui->Audio_Out->addItem(AudioOutFile);
    ui->Audio_Out->setCurrentText(AudioOutFile);

    ui->Play_Audio_In->setEnabled(true);
    ui->Stop_Audio_In->setEnabled(true);

    ui->Play_Audio_Out->setEnabled(false);
    ui->Stop_Audio_Out->setEnabled(false);

    ui->run_filter->setEnabled(true);
}

//Set Input Audio File With Combo Box
void MainWindow::on_Audio_In_currentIndexChanged(const QString &arg1)
{
    AudioInFile = AudioOutFile = arg1;
    AudioOutFile.truncate(AudioInFile.size()-3);
    AudioOutFile = AudioOutFile+"out.wav";

    ui->Audio_Out->addItem(AudioOutFile);
    ui->Audio_Out->setCurrentText(AudioOutFile);

    ui->Play_Audio_In->setEnabled(true);
    ui->Stop_Audio_In->setEnabled(true);

    ui->Play_Audio_Out->setEnabled(false);
    ui->Stop_Audio_Out->setEnabled(false);

    ui->run_filter->setEnabled(true);
}

//Set Output Audio File With Action
void MainWindow::on_Action_Output_Audio_File_triggered()
{
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save File"), "", tr("Files (*.wav)"));
    AudioOutFile = fileName;
    ui->Audio_Out->addItem(AudioOutFile);
    ui->Audio_Out->setCurrentText(AudioOutFile);

}

//Set Output Audio File With Search Button
void MainWindow::on_Search_Audio_Out_clicked()
{
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save File"), "", tr("Files (*.wav)"));
    AudioOutFile = fileName;
    ui->Audio_Out->addItem(AudioOutFile);
    ui->Audio_Out->setCurrentText(AudioOutFile);
}

//Set Output Audio File With Combo Box
void MainWindow::on_Audio_Out_currentIndexChanged(const QString &arg1)
{
    AudioOutFile = arg1;
}



//Play Audio In
void MainWindow::on_Play_Audio_In_clicked()
{
    MediaPlayer.stop();
    MediaPlayer.setMedia(QUrl::fromLocalFile(AudioInFile));
    MediaPlayer.stop();
    MediaPlayer.play();
}

//Stop Audio In
void MainWindow::on_Stop_Audio_In_clicked()
{
    MediaPlayer.stop();
}

//Play Audio Out
void MainWindow::on_Play_Audio_Out_clicked()
{
    MediaPlayer.stop();
    MediaPlayer.setMedia(QUrl::fromLocalFile(AudioOutFile));
    MediaPlayer.stop();
    MediaPlayer.play();
}

//Stop Audio In
void MainWindow::on_Stop_Audio_Out_clicked()
{
    MediaPlayer.stop();
}

//Run Filter
void MainWindow::on_run_filter_clicked()
{
    Frames = ws.getAudioSamples(AudioInFile);

    switch(ui->Filter_Box->currentIndex())
    {
        case 0:
            NoVocals(Frames);
            break;

        case 1:
            FadeIn(Frames,ui->Fade_Pan_Lenght->value());
            break;

        case 2:
            FadeOut(Frames,ui->Fade_Pan_Lenght->value());
            break;

        case 3:
            Left2Right(Frames,ui->Fade_Pan_Lenght->value());
            break;
    };

    cout<<endl<<"running filter"<<endl;
    cout<<endl<<AudioOutFile.toStdString()<<endl;

    ws.writeNewAudioFile(AudioOutFile);
    ui->Play_Audio_Out->setEnabled(true);
    ui->Stop_Audio_Out->setEnabled(true);
}









