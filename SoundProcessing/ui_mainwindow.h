/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.3.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *Action_Input_Audio_File;
    QAction *Action_Output_Audio_File;
    QAction *actionExit;
    QAction *actionAbout;
    QAction *actionInstructions;
    QAction *actionRemove_Vocals;
    QAction *actionFade_In;
    QAction *actionFade_Out;
    QAction *actionSwap_Speaker_Side;
    QWidget *centralWidget;
    QWidget *layoutWidget;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_3;
    QSpinBox *Fade_Pan_Lenght;
    QWidget *layoutWidget1;
    QVBoxLayout *verticalLayout;
    QLabel *Audio_In_Label;
    QLabel *Audio_Out_Label;
    QLabel *Current_Filter_Label;
    QWidget *layoutWidget2;
    QVBoxLayout *verticalLayout_2;
    QComboBox *Audio_In;
    QComboBox *Audio_Out;
    QComboBox *Filter_Box;
    QWidget *layoutWidget3;
    QVBoxLayout *verticalLayout_3;
    QPushButton *Search_Audio_In;
    QPushButton *Search_Audio_Out;
    QPushButton *run_filter;
    QFrame *frame_2;
    QLabel *old_label_3;
    QPushButton *Stop_Audio_Out;
    QPushButton *Play_Audio_Out;
    QPushButton *Play_Audio_In;
    QPushButton *Stop_Audio_In;
    QFrame *frame;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QMenu *menuHelp;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(581, 334);
        Action_Input_Audio_File = new QAction(MainWindow);
        Action_Input_Audio_File->setObjectName(QStringLiteral("Action_Input_Audio_File"));
        Action_Output_Audio_File = new QAction(MainWindow);
        Action_Output_Audio_File->setObjectName(QStringLiteral("Action_Output_Audio_File"));
        Action_Output_Audio_File->setEnabled(true);
        actionExit = new QAction(MainWindow);
        actionExit->setObjectName(QStringLiteral("actionExit"));
        actionAbout = new QAction(MainWindow);
        actionAbout->setObjectName(QStringLiteral("actionAbout"));
        actionInstructions = new QAction(MainWindow);
        actionInstructions->setObjectName(QStringLiteral("actionInstructions"));
        actionRemove_Vocals = new QAction(MainWindow);
        actionRemove_Vocals->setObjectName(QStringLiteral("actionRemove_Vocals"));
        actionFade_In = new QAction(MainWindow);
        actionFade_In->setObjectName(QStringLiteral("actionFade_In"));
        actionFade_Out = new QAction(MainWindow);
        actionFade_Out->setObjectName(QStringLiteral("actionFade_Out"));
        actionSwap_Speaker_Side = new QAction(MainWindow);
        actionSwap_Speaker_Side->setObjectName(QStringLiteral("actionSwap_Speaker_Side"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        layoutWidget = new QWidget(centralWidget);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(10, 170, 221, 29));
        horizontalLayout_2 = new QHBoxLayout(layoutWidget);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(0, 0, 0, 0);
        label_3 = new QLabel(layoutWidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setEnabled(false);

        horizontalLayout_2->addWidget(label_3);

        Fade_Pan_Lenght = new QSpinBox(layoutWidget);
        Fade_Pan_Lenght->setObjectName(QStringLiteral("Fade_Pan_Lenght"));
        Fade_Pan_Lenght->setMinimum(44100);
        Fade_Pan_Lenght->setMaximum(220500);
        Fade_Pan_Lenght->setSingleStep(44100);

        horizontalLayout_2->addWidget(Fade_Pan_Lenght);

        layoutWidget1 = new QWidget(centralWidget);
        layoutWidget1->setObjectName(QStringLiteral("layoutWidget1"));
        layoutWidget1->setGeometry(QRect(10, 70, 101, 91));
        verticalLayout = new QVBoxLayout(layoutWidget1);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        Audio_In_Label = new QLabel(layoutWidget1);
        Audio_In_Label->setObjectName(QStringLiteral("Audio_In_Label"));

        verticalLayout->addWidget(Audio_In_Label);

        Audio_Out_Label = new QLabel(layoutWidget1);
        Audio_Out_Label->setObjectName(QStringLiteral("Audio_Out_Label"));

        verticalLayout->addWidget(Audio_Out_Label);

        Current_Filter_Label = new QLabel(layoutWidget1);
        Current_Filter_Label->setObjectName(QStringLiteral("Current_Filter_Label"));

        verticalLayout->addWidget(Current_Filter_Label);

        layoutWidget2 = new QWidget(centralWidget);
        layoutWidget2->setObjectName(QStringLiteral("layoutWidget2"));
        layoutWidget2->setGeometry(QRect(110, 70, 361, 95));
        verticalLayout_2 = new QVBoxLayout(layoutWidget2);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        Audio_In = new QComboBox(layoutWidget2);
        Audio_In->setObjectName(QStringLiteral("Audio_In"));
        Audio_In->setEditable(true);

        verticalLayout_2->addWidget(Audio_In);

        Audio_Out = new QComboBox(layoutWidget2);
        Audio_Out->setObjectName(QStringLiteral("Audio_Out"));
        Audio_Out->setEditable(true);

        verticalLayout_2->addWidget(Audio_Out);

        Filter_Box = new QComboBox(layoutWidget2);
        Filter_Box->setObjectName(QStringLiteral("Filter_Box"));
        Filter_Box->setMouseTracking(false);
        Filter_Box->setFocusPolicy(Qt::ClickFocus);
        Filter_Box->setContextMenuPolicy(Qt::DefaultContextMenu);
        Filter_Box->setAutoFillBackground(false);
        Filter_Box->setEditable(false);
        Filter_Box->setFrame(true);
        Filter_Box->setModelColumn(0);

        verticalLayout_2->addWidget(Filter_Box);

        layoutWidget3 = new QWidget(centralWidget);
        layoutWidget3->setObjectName(QStringLiteral("layoutWidget3"));
        layoutWidget3->setGeometry(QRect(470, 70, 104, 100));
        verticalLayout_3 = new QVBoxLayout(layoutWidget3);
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(0, 0, 0, 0);
        Search_Audio_In = new QPushButton(layoutWidget3);
        Search_Audio_In->setObjectName(QStringLiteral("Search_Audio_In"));
        Search_Audio_In->setStyleSheet(QLatin1String("background-color: gray;\n"
"border-style: outset;\n"
"border-width: 2px; \n"
"border-color:gray;\n"
"color:white;\n"
"\n"
""));

        verticalLayout_3->addWidget(Search_Audio_In);

        Search_Audio_Out = new QPushButton(layoutWidget3);
        Search_Audio_Out->setObjectName(QStringLiteral("Search_Audio_Out"));
        Search_Audio_Out->setStyleSheet(QLatin1String("background-color: gray;\n"
"border-style: outset;\n"
"border-width: 2px; \n"
"border-color:gray;\n"
"color:white;\n"
"\n"
""));

        verticalLayout_3->addWidget(Search_Audio_Out);

        run_filter = new QPushButton(layoutWidget3);
        run_filter->setObjectName(QStringLiteral("run_filter"));
        run_filter->setEnabled(false);
        run_filter->setStyleSheet(QLatin1String("background-color: gray;\n"
"border-style: outset;\n"
"border-width: 2px; \n"
"border-color:gray;\n"
"color:white;\n"
"\n"
""));

        verticalLayout_3->addWidget(run_filter);

        frame_2 = new QFrame(centralWidget);
        frame_2->setObjectName(QStringLiteral("frame_2"));
        frame_2->setGeometry(QRect(-110, -30, 861, 91));
        frame_2->setStyleSheet(QLatin1String("background-color:#c2050b\n"
";\n"
"border-color:#525662;"));
        frame_2->setFrameShape(QFrame::StyledPanel);
        frame_2->setFrameShadow(QFrame::Raised);
        old_label_3 = new QLabel(frame_2);
        old_label_3->setObjectName(QStringLiteral("old_label_3"));
        old_label_3->setGeometry(QRect(330, 30, 181, 61));
        QFont font;
        font.setPointSize(25);
        old_label_3->setFont(font);
        old_label_3->setStyleSheet(QStringLiteral("color:#f9ffff;"));
        Stop_Audio_Out = new QPushButton(centralWidget);
        Stop_Audio_Out->setObjectName(QStringLiteral("Stop_Audio_Out"));
        Stop_Audio_Out->setEnabled(false);
        Stop_Audio_Out->setGeometry(QRect(190, 260, 171, 41));
        Stop_Audio_Out->setStyleSheet(QLatin1String("background-color: gray;\n"
"border-style: outset;\n"
"border-width: 2px; \n"
"border-color:gray;\n"
"color:white;\n"
"\n"
""));
        Play_Audio_Out = new QPushButton(centralWidget);
        Play_Audio_Out->setObjectName(QStringLiteral("Play_Audio_Out"));
        Play_Audio_Out->setEnabled(false);
        Play_Audio_Out->setGeometry(QRect(190, 210, 171, 41));
        Play_Audio_Out->setStyleSheet(QLatin1String("background-color: gray;\n"
"border-style: outset;\n"
"border-width: 2px; \n"
"border-color:gray;\n"
"color:white;\n"
"\n"
""));
        Play_Audio_In = new QPushButton(centralWidget);
        Play_Audio_In->setObjectName(QStringLiteral("Play_Audio_In"));
        Play_Audio_In->setEnabled(false);
        Play_Audio_In->setGeometry(QRect(10, 210, 171, 41));
        Play_Audio_In->setStyleSheet(QLatin1String("background-color: gray;\n"
"border-style: outset;\n"
"border-width: 2px; \n"
"border-color:gray;\n"
"color:white;\n"
"\n"
""));
        Stop_Audio_In = new QPushButton(centralWidget);
        Stop_Audio_In->setObjectName(QStringLiteral("Stop_Audio_In"));
        Stop_Audio_In->setEnabled(false);
        Stop_Audio_In->setGeometry(QRect(10, 260, 171, 41));
        Stop_Audio_In->setStyleSheet(QLatin1String("background-color: gray;\n"
"border-style: outset;\n"
"border-width: 2px; \n"
"border-color:gray;\n"
"color:white;\n"
"\n"
""));
        frame = new QFrame(centralWidget);
        frame->setObjectName(QStringLiteral("frame"));
        frame->setGeometry(QRect(370, 250, 201, 51));
        frame->setStyleSheet(QLatin1String("background-image: url(/home/rgb/Desktop/eip/CaesarCipher/logo.png);\n"
"border: 0px;\n"
"repeat: no-repeat;"));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        MainWindow->setCentralWidget(centralWidget);
        layoutWidget->raise();
        layoutWidget->raise();
        layoutWidget->raise();
        layoutWidget->raise();
        frame_2->raise();
        Stop_Audio_Out->raise();
        Play_Audio_Out->raise();
        Play_Audio_In->raise();
        Stop_Audio_In->raise();
        Search_Audio_In->raise();
        frame->raise();
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 581, 25));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QStringLiteral("menuFile"));
        menuHelp = new QMenu(menuBar);
        menuHelp->setObjectName(QStringLiteral("menuHelp"));
        MainWindow->setMenuBar(menuBar);

        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuHelp->menuAction());
        menuFile->addAction(Action_Input_Audio_File);
        menuFile->addAction(Action_Output_Audio_File);
        menuFile->addSeparator();
        menuFile->addAction(actionExit);
        menuHelp->addAction(actionAbout);

        retranslateUi(MainWindow);

        Filter_Box->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Wave Editor", 0));
        Action_Input_Audio_File->setText(QApplication::translate("MainWindow", "Input Audio File", 0));
        Action_Output_Audio_File->setText(QApplication::translate("MainWindow", "Output Audio File", 0));
        actionExit->setText(QApplication::translate("MainWindow", "Exit", 0));
        actionAbout->setText(QApplication::translate("MainWindow", "About", 0));
        actionInstructions->setText(QApplication::translate("MainWindow", "Instructions", 0));
        actionRemove_Vocals->setText(QApplication::translate("MainWindow", "Remove Vocals", 0));
        actionFade_In->setText(QApplication::translate("MainWindow", "Fade In", 0));
        actionFade_Out->setText(QApplication::translate("MainWindow", "Fade Out", 0));
        actionSwap_Speaker_Side->setText(QApplication::translate("MainWindow", "Swap Speaker Side", 0));
        label_3->setText(QApplication::translate("MainWindow", "Pan/Fade Length:", 0));
        Audio_In_Label->setText(QApplication::translate("MainWindow", "Audio In:", 0));
        Audio_Out_Label->setText(QApplication::translate("MainWindow", "Audio Out:", 0));
        Current_Filter_Label->setText(QApplication::translate("MainWindow", "Current Filter:", 0));
        Filter_Box->clear();
        Filter_Box->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "Remove Vocals", 0)
         << QApplication::translate("MainWindow", "Fade In", 0)
         << QApplication::translate("MainWindow", "Fade Out", 0)
         << QApplication::translate("MainWindow", "Pan Left to Right", 0)
        );
        Search_Audio_In->setText(QApplication::translate("MainWindow", "Search", 0));
        Search_Audio_Out->setText(QApplication::translate("MainWindow", "Search", 0));
        run_filter->setText(QApplication::translate("MainWindow", "Run Filter", 0));
        old_label_3->setText(QApplication::translate("MainWindow", "Sounds Lab", 0));
        Stop_Audio_Out->setText(QApplication::translate("MainWindow", "Stop Audio Out", 0));
        Play_Audio_Out->setText(QApplication::translate("MainWindow", "Play Audio Out", 0));
        Play_Audio_In->setText(QApplication::translate("MainWindow", "Play Audio In", 0));
        Stop_Audio_In->setText(QApplication::translate("MainWindow", "Stop Audio In", 0));
        menuFile->setTitle(QApplication::translate("MainWindow", "File", 0));
        menuHelp->setTitle(QApplication::translate("MainWindow", "Help", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
